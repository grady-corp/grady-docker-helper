# Grady Docker Helper

A small script to help with the creation of a new Grady instance.  

## Usage
Display help:
```
python3 create-instance.py -h
```

Create a new instance folder with all necessary files:
```
python3 create-instance.py sose 18 ckurs lecturer 1.0.0 ..
```
The last argument is the path at which the folder is created, default: .. 
